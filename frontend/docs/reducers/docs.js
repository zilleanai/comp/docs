import { listDocs } from 'comps/docs/actions'


export const KEY = 'docs'

const initialState = {
  isLoading: false,
  isLoaded: false,
  names: [],
  byname: {},
  error: null,
}

export default function(state = initialState, action) {
  const { type, payload } = action
  const { docs } = payload || {}
  switch (type) {
    case listDocs.REQUEST:
      return { ...state,
        isLoading: true,
      }

    case listDocs.SUCCESS:
      return { ...state,
        isLoaded: true,
        names: docs.map((doc) => doc),
        byname: docs.reduce((byname, doc) => {
          byname[doc] = doc
          return byname
        }, {})
      }

    case listDocs.FULFILL:
      return { ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectDocs = (state) => state[KEY]
export const selectDocsList = (state) => {
  const docs = selectDocs(state)
  return docs.names.map((doc) => docs.byname[doc])
}
