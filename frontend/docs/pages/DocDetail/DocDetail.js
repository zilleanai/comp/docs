import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { className } from 'classnames'
import { bindRoutineCreators } from 'actions'
import { PageContent } from 'components'
import { injectReducer, injectSagas } from 'utils/async'
import ReactMarkdown from 'react-markdown';
import fetch from 'isomorphic-fetch'
import { v1 } from 'api'
import './doc-detail.css'

class DocDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
    }
  }

  componentDidMount() {
    const { doc } = this.props
    const url = v1(`/docs/${doc}`)
    fetch(url).then((resp) => resp.text().then(
      (data) => {
        this.setState({ data })
      }
    ))
  }


  componentWillReceiveProps(nextProps) {
  }

  render() {
    const { isLoaded, doc } = this.props
    const { data } = this.state
    if (!isLoaded) {
      return null
    }
    return (
      <PageContent className='doc-page'>
        <Helmet>
          <title>Doc: {doc}</title>
        </Helmet>
        <ReactMarkdown source={data} />
      </PageContent>
    )
  }
}


const withConnect = connect(
  (state, props) => {
    const name = props.match.params.name
    const doc = name
    return {
      name,
      doc,
      isLoaded: !!doc,
    }
  }
)

export default compose(
  withConnect,
)(DocDetail)
