import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import { PageContent } from 'components'
import { injectReducer, injectSagas } from 'utils/async'
import { listDocs } from 'comps/docs/actions'
import { selectDocsList } from 'comps/docs/reducers/docs'
import { DocLink } from 'comps/docs/components'

class Docs extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const { listDocs } = this.props
    listDocs.maybeTrigger()
  }

  render() {
    const { error, docs } = this.props
    if (error) {
      return null
    }
    return (
      <PageContent className='docs-page'>
        <Helmet>
          <title>Docs</title>
        </Helmet>
        <h1>Docs</h1>
        <ul className="docs-list">
          {docs.map((doc, i) => {
            return (
              <li key={i}>
                <span>
                <DocLink doc={doc} />
                </span>
              </li>
            )
          })}
        </ul>
      </PageContent>
    )
  }
}

const withReducer = injectReducer(require('comps/docs/reducers/docs'))

const withSagas = injectSagas(require('comps/docs/sagas/docs'))

const withConnect = connect(
  (state) => ({ docs: selectDocsList(state) }),
  (dispatch) => bindRoutineCreators({ listDocs }, dispatch),
)

export default compose(
  withReducer,
  withSagas,
  withConnect,
)(Docs)
