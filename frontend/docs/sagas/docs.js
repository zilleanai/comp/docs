import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listDocs } from 'comps/docs/actions'
import DocsApi from 'comps/docs/api'
import { selectDocs } from 'comps/docs/reducers/docs'


export const KEY = 'docs'

export const maybeListDocsSaga = function *() {
  const { isLoading, isLoaded } = yield select(selectDocs)
  if (!(isLoaded || isLoading)) {
    yield put(listDocs.trigger())
  }
}

export const listDocsSaga = createRoutineSaga(
  listDocs,
  function *successGenerator() {
    const docs = yield call(DocsApi.listDocs)
console.log(docs['modules'])
    yield put(listDocs.success({ 'docs': docs['modules'] }))
  }
)

export default () => [
  takeEvery(listDocs.MAYBE_TRIGGER, maybeListDocsSaga),
  takeLatest(listDocs.TRIGGER, listDocsSaga),
]
