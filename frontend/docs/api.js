import { get, post } from 'utils/request'
import { v1 } from 'api'


function docs(uri) {
  return v1(`/docs${uri}`)
}

export default class Docs {

  static listDocs() {
    return get(docs('/'))
  }

}
