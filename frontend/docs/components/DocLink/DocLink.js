import React from 'react'

import { NavLink } from 'components'
import { ROUTES } from 'routes'


export default ({ doc }) =>
  <NavLink to={ROUTES.DocDetail} params={{ name: doc }}>
    {doc}
  </NavLink>
