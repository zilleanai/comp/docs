import os
import sys
import json
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable, bundles
from backend.config import Config as AppConfig

from werkzeug.utils import secure_filename


class DocsController(Controller):

    @route('/<string:module>')
    def index(self, module):
        if module:
            m = current_app.unchained.docs_bundle.bundles[module]
            return send_file(m().index())
        return abort(404)

    @route('/<string:module>/<path:path>')
    def path(self, module, path):
        if module:
            m = current_app.unchained.docs_bundle.bundles[module]
            file_path = m().path(path)
            if not file_path: return abort(404)
            return send_file(file_path)
        return abort(404)

    @route('/')
    def modules(self):
        modules = []
        for bundle in current_app.unchained.docs_bundle.bundles:
            module = current_app.unchained.docs_bundle.bundles[bundle].__module__
            modules.append(bundle)
        resp = jsonify(modules=modules)
        return resp
