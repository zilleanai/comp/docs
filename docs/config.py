from flask_unchained import BundleConfig


class Config(BundleConfig):
    """
    Default configuration options for the docs Bundle.
    """
    pass
