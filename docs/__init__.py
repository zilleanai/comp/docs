from flask_unchained import Bundle

class DocsBundle(Bundle):
    bundles = {}
    name = 'docs_bundle'
    """
    The :class:`Bundle` subclass for the docs bundle. Has no special behavior.
    """
    command_group_names = ['docs']
