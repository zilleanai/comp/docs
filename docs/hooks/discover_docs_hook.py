import inspect
from flask_unchained import AppFactoryHook, FlaskUnchained
from typing import *


class DiscoverDocsHook(AppFactoryHook):
    """
    Discovers docs.
    """
    name = 'docs'
    bundle_module_name = 'docs'


    def process_objects(self, app: FlaskUnchained, objects: Dict[str, Any]):
        for name, module in objects.items():
            print('docs bundle', name)
            self.bundle.bundles[name] = module

    def type_check(self, obj):
        if not inspect.isclass(obj):
            return False
        return True
